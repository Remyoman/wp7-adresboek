﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace WP7_Adresboek
{
    public partial class NewContact
    {
        private readonly Controller _control;

        public NewContact()
        {
            _control = App.Control;
            InitializeComponent();
        }

        private void EraseFields(object sender, RoutedEventArgs e)
        {
            Surname.Text = "Voornaam";
            LastName.Text = "Achternaam";
            Phone.Text = "Telefoonnummer";
            Mail.Text = "*E-mail";
            Comments.Text = "*Opmerkingen";
        }

        private void SaveContact(object sender, RoutedEventArgs e)
        {
            if (Surname.Text == "" || LastName.Text == "" || Phone.Text == "")
                MessageBox.Show("Please fill in all of the required fields.");
            else
            {
                _control.AddContact(Surname.Text, LastName.Text, Convert.ToUInt64(Phone.Text), Mail.Text, Comments.Text);
                MessageBox.Show("Contact succesfully added.");
                EraseFields(sender, e);
            }
        }

        private void EraseText(object sender, RoutedEventArgs e)
        {
            TextBox selectedBox = (TextBox)sender;
            selectedBox.Text = "";
        }
    }
}