﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Device.Location;
using System.Linq;

namespace WP7_Adresboek
{
    public class Controller
    {
        private readonly DatabaseContext _databaseContext;
        public DatabaseContext DatabaseContext
        {
            get { return _databaseContext; }
        }
        private ObservableCollection<ContactsTable> _contactsTables;
        public ObservableCollection<ContactsTable> ContactsTables
        {
            get
            {
                return _contactsTables;
            }
            set
            {
                if(_contactsTables != value)
                {
                    NotifyPropertyChanging("ContactsTable");
                    _contactsTables = value;
                    NotifyPropertyChanged("ContactsTable");
                }
            }
        }
        private ObservableCollection<HistoryTable> _historyTables;
        public ObservableCollection<HistoryTable> HistoryTables
        {
            get
            {
                return _historyTables;
            }
            set
            {
                if(_historyTables != value)
                {
                    NotifyPropertyChanging("HistoryTable");
                    _historyTables = value;
                    NotifyPropertyChanged("HistoryTable");
                }
            }
        }

        public Controller()
        {
            _contactsTables= new ObservableCollection<ContactsTable>();
            ContactsTables = new ObservableCollection<ContactsTable>();
            _historyTables = new ObservableCollection<HistoryTable>();
            HistoryTables = new ObservableCollection<HistoryTable>();
            _databaseContext = new DatabaseContext();
            if(!_databaseContext.DatabaseExists())
            {
                _databaseContext.CreateDatabase();
                DatabaseDebugData();
            }
        }

        private void DatabaseDebugData()
        {
            ContactsTable contact = new ContactsTable { FirstName = "Henk", LastName = "Smit", PhoneNumber = 069784512, MailAddress = "henksmit@live.nl", Comments = "" };
            _databaseContext.Contacts.InsertOnSubmit(contact);
            HistoryTable historyItem = new HistoryTable { Timestamp = DateTime.Now, Latitude = 51.573015481652675, Longitude = 4.781455993652344, Modifications = "HenkSmit added." };
            _databaseContext.History.InsertOnSubmit(historyItem);
            contact = new ContactsTable { FirstName = "Jaap", LastName = "Beeksman", PhoneNumber = 066147985, MailAddress = "j.beeksman@yahoo.com", Comments = "Eigenaar" };
            _databaseContext.Contacts.InsertOnSubmit(contact);
            historyItem = new HistoryTable { Timestamp = DateTime.Now, Latitude = 51.582763, Longitude = 4.798343, Modifications = "JaapBeeksman added." };
            _databaseContext.History.InsertOnSubmit(historyItem);
            contact = new ContactsTable { FirstName = "Willem", LastName = "Schaap", PhoneNumber = 068497613, MailAddress = "willem.s@gmail.com", Comments = "Vader" };
            _databaseContext.Contacts.InsertOnSubmit(contact);
            historyItem = new HistoryTable { Timestamp = DateTime.Now, Latitude = 51.586696, Longitude = 4.780855, Modifications = "WillemSchaap added." };
            _databaseContext.History.InsertOnSubmit(historyItem);
            contact = new ContactsTable { FirstName = "Jan", LastName = "Jaap", PhoneNumber = 09874519, MailAddress = "jan.jaap@schaap.nl", Comments = "Schapenherder" };
            _databaseContext.Contacts.InsertOnSubmit(contact);
            historyItem = new HistoryTable { Timestamp = DateTime.Now, Latitude = 51.573224, Longitude = 4.788746, Modifications = "JanJaap added." };
            _databaseContext.History.InsertOnSubmit(historyItem);
            _databaseContext.SubmitChanges();
            var debugContactsQuery = from ContactsTable contactsTable in _databaseContext.Contacts select contactsTable;
            ContactsTables = new ObservableCollection<ContactsTable>(debugContactsQuery);
            var debugHistoryQuery = from HistoryTable historyTable in _databaseContext.History select historyTable;
            HistoryTables = new ObservableCollection<HistoryTable>(debugHistoryQuery);
        }

        public void DeleteContact(ContactsTable contact)
        {
            _contactsTables.Remove(contact);
            _databaseContext.Contacts.DeleteOnSubmit(contact);
            HistoryTable newHistory = new HistoryTable { Timestamp = DateTime.Now, Latitude = Map.RequestCurrentLocation().Latitude, Longitude = Map.RequestCurrentLocation().Latitude, Modifications = contact.FirstName + contact.LastName + " removed." };
            _databaseContext.History.InsertOnSubmit(newHistory);
            _databaseContext.SubmitChanges();
        }

        public void DeleteHistory(HistoryTable history)
        {
            _historyTables.Remove(history);
            _databaseContext.History.DeleteOnSubmit(history);
            _databaseContext.SubmitChanges();
        }

        public void AddContact(string name, string lastName, ulong phone, string mail, string comments)
        {
            ContactsTable newContact = new ContactsTable { FirstName = name, LastName = lastName, PhoneNumber = phone, MailAddress = mail, Comments = comments };
            _databaseContext.Contacts.InsertOnSubmit(newContact);
            HistoryTable newHistory = new HistoryTable { Timestamp = DateTime.Now, Latitude = Map.RequestCurrentLocation().Latitude, Longitude = Map.RequestCurrentLocation().Longitude, Modifications = name + lastName + " added." };
            _databaseContext.History.InsertOnSubmit(newHistory);
            _databaseContext.SubmitChanges();
            var addContactsQuery = from ContactsTable contactsTable in _databaseContext.Contacts select contactsTable;
            ContactsTables = new ObservableCollection<ContactsTable>(addContactsQuery);
            var addHistoryQuery = from HistoryTable historyTable in _databaseContext.History select historyTable;
            HistoryTables = new ObservableCollection<HistoryTable>(addHistoryQuery);
        }

        public IList<ContactsTable> LoadContactList()
        {
            List<ContactsTable> _contacts = new List<ContactsTable>();
            var contactListQuery = from contacts in _databaseContext.Contacts select contacts;
            _contacts = contactListQuery.ToList();
            return _contacts;
        }

        public IList<HistoryTable> LoadHistoryList()
        {
            List<HistoryTable> _history = new List<HistoryTable>();
            var contactListQuery = from history in _databaseContext.History select history;
            _history = contactListQuery.ToList();
            return _history;
        }

        #region INotifyProperty Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }
        #endregion
    }
}
