﻿using System.Windows;

namespace WP7_Adresboek
{
    public partial class Contacts
    {
        private readonly Controller _control;

        public Contacts()
        {
            _control = App.Control;
            InitializeComponent();
            ContactsList.ItemsSource = _control.LoadContactList();
        }

        private void DeleteContact(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var panel = (FrameworkElement)sender;
            var item = (FrameworkElement)e.OriginalSource;
            ContactsTable contact = (ContactsTable)item.DataContext;
            if(contact != null)
            {
                _control.DeleteContact(contact);
                ContactsList.ItemsSource = _control.LoadContactList();
                MessageBox.Show("Contact " + contact.FirstName + " " + contact.LastName + "is verwijderd.");
            }
        }
    }
}