﻿using System.Data.Linq;

namespace WP7_Adresboek
{
    public class DatabaseContext: DataContext
    {
        public static string DbConnectionString = "Data Source=isostore:/adresboek.sdf";

        public DatabaseContext(): base(DbConnectionString)
        {

        }
        public Table<ContactsTable> Contacts;
        public Table<HistoryTable> History;
    }
}
