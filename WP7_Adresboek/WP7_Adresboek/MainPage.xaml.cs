﻿using System;
using System.Windows;

namespace WP7_Adresboek
{
    public partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void GoToHistory(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/History.xaml", UriKind.Relative));
        }

        private void GoToNewContact(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewContact.xaml", UriKind.Relative));
        }

        private void GoToContacts(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Contacts.xaml", UriKind.Relative));
        }

        private void GoToMap(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Map.xaml", UriKind.Relative));
        }
    }
}