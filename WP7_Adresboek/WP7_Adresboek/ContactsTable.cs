﻿using System.ComponentModel;
using System.Data.Linq.Mapping;
using Microsoft.Phone.Data.Linq.Mapping;

namespace WP7_Adresboek
{
    [Index(Columns = "FirstName, LastName", Name = "UniqueContactIndex", IsUnique = true)]
    [Table]
    public class ContactsTable: INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _id;
        private string _firstName;
        private string _lastName;
        private ulong _phoneNumber;
        private string _mailAddress;
        private string _comments;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int Id
        {
            get { return _id; }
            set
            {
                if(_id != value)
                {
                    NotifyPropertyChanging("ID");
                    _id = value;
                    NotifyPropertyChanged("ID");
                }
            }
        }

        [Column]
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if(_firstName != value)
                {
                    NotifyPropertyChanging("FirstName");
                    _firstName = value;
                    NotifyPropertyChanged("FirstName");
                }
            }
        }

        [Column]
        public string LastName
        {
            get { return _lastName; }
            set
            {
                if(_lastName != value)
                {
                    NotifyPropertyChanging("LastName");
                    _lastName = value;
                    NotifyPropertyChanged("LastName");
                }
            }
        }

        [Column]
        public ulong PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                if(_phoneNumber != value)
                {
                    NotifyPropertyChanging("PhoneNumber");
                    _phoneNumber = value;
                    NotifyPropertyChanged("PhoneNumber");
                }
            }
        }

        [Column]
        public string MailAddress
        {
            get { return _mailAddress; }
            set
            {
                if(_mailAddress != value)
                {
                    NotifyPropertyChanging("MailAddress");
                    _mailAddress = value;
                    NotifyPropertyChanged("MailAddress");
                }
            }
        }

        [Column]
        public string Comments
        {
            get { return _comments; }
            set
            {
                if(_comments != value)
                {
                    NotifyPropertyChanging("Comments");
                    _comments = value;
                    NotifyPropertyChanged("Comments");
                }
            }
        }

        #region Inherited Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }
        #endregion
    }
}
