﻿using System.Windows;

namespace WP7_Adresboek
{
    public partial class History
    {
        private readonly Controller _control;

        public History()
        {
            _control = App.Control;
            InitializeComponent();
            HistoryList.ItemsSource = _control.LoadHistoryList();
        }

        private void DeleteHistory(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var panel = (FrameworkElement)sender;
            var item = (FrameworkElement)e.OriginalSource;
            HistoryTable history = (HistoryTable)item.DataContext;
            if (history != null)
            {
                _control.DeleteHistory(history);
                HistoryList.ItemsSource = _control.LoadHistoryList();
                MessageBox.Show("Geschiedenis item " + history.Id + " is verwijderd.");
            }
        }
    }
}