﻿using System;
using System.ComponentModel;
using System.Data.Linq.Mapping;

namespace WP7_Adresboek
{
    [Table]
    public class HistoryTable: INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _id;
        private DateTime _timestamp;
        private double _latitude;
        private double _longitude;
        private string _modifications;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int Id
        {
            get { return _id; }
            set
            {
                if(_id != value)
                {
                    NotifyPropertyChanging("ID");
                    _id = value;
                    NotifyPropertyChanged("ID");
                }
            }
        }

        [Column]
        public DateTime Timestamp
        {
            get { return _timestamp; }
            set
            {
                if(_timestamp != value)
                {
                    NotifyPropertyChanging("Timestamp");
                    _timestamp = value;
                    NotifyPropertyChanged("Timestamp");
                }
            }
        }

        [Column]
        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if(_latitude != value)
                {
                    NotifyPropertyChanging("Latitude");
                    _latitude = value;
                    NotifyPropertyChanged("Latitude");
                }
            }
        }

        [Column]
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if(_longitude != value)
                {
                    NotifyPropertyChanging("Longitude");
                    _longitude = value;
                    NotifyPropertyChanged("Longitude");
                }
            }
        }

        [Column]
        public string Modifications
        {
            get { return _modifications; }
            set
            {
                if (_modifications != value)
                {
                    NotifyPropertyChanging("Modifications");
                    _modifications = value;
                    NotifyPropertyChanged("Modifications");
                }
            }
        }

        #region Inherited Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }
        #endregion
    }
}
